<!-- Title of issue should be: Heroes Coffee Chat: YYYY/MM/DD TIMEUTC Ex: "Heroes Coffee Chat: 2021/03/17 2000UTC" -->

Please use this template when proposing or scheduling a GitLab Heroes coffee chat.

## Coffee Chat Details

* **Date**: 
* **Time**:
* **Link to join**: 
* **Notes**:

## Tasks

* [ ] Assign this issue to the host 
* [ ] Create meeting link or request a Zoom link from @jrachel1 in a comment on this issue 
* [ ] Please share in the GitLab Heroes channel on Gitter: [Heroes channel](https://gitter.im/gitlab/heroes )

/assign @johncoghlan @jrachel1
/label ~Heroes 
/confidential  <!-- These issues are confidential to limit risk of Zoom bombing -->



