<!-- Title of issue should be: MONTH YEAR Heroes Update. Ex: "September 2020 Heroes Update" -->

< Brief introduction in paragraph form.>

## Topic 1

Update on topic. 

## Topic 2 

Update on topic. 

## Reminders

- Join our channel on Gitter: [Heroes channel](https://gitter.im/gitlab/heroes )
- Add yourself to the Heroes page: [Instructions](https://about.gitlab.com/handbook/marketing/community-relations/evangelist-program/#adding-yourself-to-the-heroes-page)
- Additional relevant links and resources. 

## Thanks

Thank you section and shout outs. 
