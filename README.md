# GitLab Heroes

At GitLab, we believe everyone can contribute. In that spirit, we believe everyone in our community can become a GitLab Hero. Whether you are [organizing meetups](https://about.gitlab.com/community/meetups), recording demos for YouTube, giving talks at conferences and events, writing technical blog posts, or [contributing to our open source project](https://about.gitlab.com/community/contribute), we want to engage, support, and recognize you for your contribution.

# Governance

This program is currently managed by GitLab's Evangelist Program Manager. A steering committee has been formed to determine whether and how we can establish a Heroes leadership team that owns goal setting, strategy, recruiting new members, reviewing applications, coaching and supporting new Heroes, and other program-related responsibilities.

The steering commitee is composed of current Heroes, including: 
* [Abubukar Hassan](https://gitlab.com/itssadon)
* [Adrian Moisey](https://gitlab.com/adrianmoisey)
* [Marvin Karegyeya](https://gitlab.com/nuwe1)
* [Nico Meisenzahl](https://gitlab.com/nico-meisenzahl)
* [Vladimir Roudakov](https://gitlab.com/VladimirAus)
* [James Tharpre (observer)](https://gitlab.com/jamestharpe-tmo)

The steering committee meets on the last Wednesday of every month at 12:30 UTC. Recordings of the meetings are shared on YouTube and the agendas and meeting notes can be found in a [Google Doc](https://docs.google.com/document/d/1iPfZXv1jI5qRlV7tCT0TbiRu_RywyuaL2vEb0etUe94/edit?usp=sharing). 

# Why become a GitLab Hero

GitLab Hero is designed to help our community members grow and develop. We want to invest in the professional development of Heroes, help them develop new skills like community organizing and public speaking, and turn them into advocates for best practices in the technology community so that they can help others grow and develop in their understanding of open source, DevOps, and software development.  

# Hero Rewards
* Invitations to special events including GitLab Commit
* Support for travel to speak about GitLab at events
* GitLab Gold and Ultimate licenses
* Special Heroes swag so people know how awesome you are
* Access to GitLab's product, engineering, and technical evangelism teams to help with reviews of talks and blog posts

# Attributes of a Hero
* Possess a passion for GitLab, DevOps, and Open Source software.
* Aspire to our mission, uphold our values, and adhere to our Code of Conduct.
* Sustained level of contribution to GitLab as a code contributor, community organizer, or thought leader.

# Levels 
The Heroes program has three levels (Contributor, Hero, and Superhero) with corresponding contributions and rewards which can be found on our [GitLab Heroes](https://about.gitlab.com/community/heroes/#heros-journey) page. 

# Nominations
Nominate yourself or another member of the community by visiting the [GitLab Heroes](https://about.gitlab.com/community/heroes/#apply) page.

# Contributions 

Heroes are encouraged to create issues when working on contributions to the GitLab community. Using issues help us to better collaborate with our Heroes and provides greater transparency to the GitLab community. 

The following templates can be used to share your contributions: 

* When working on a CFP submission, tech talk, blog post, video, or other content, please use the [Heroes Content](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes/issues/new?issuable_template=heroes-content) issue template.
* When organizing a meetup, please use the [Meetup Organizer](https://gitlab.com/gitlab-com/marketing/community-relations/evangelist-program/general/issues/new?issuable_template=meetup-organizer) issue template. 
* When contributing to GitLab, visit our [Contribute to GitLab Page](https://about.gitlab.com/community/contribute/) for more info. 

## Adding a contribution to the GitLab Heroes Contributions page

To add a contribution to the [GitLab Heroes Contributions](https://about.gitlab.com/community/heroes/contributions/) page, please follow these steps:

1. Skip to step 5 if you already have a fork of www-gitlab-com. 
2. Go to the [recent_contributions file](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/heroes_contributions.yml) in the GitLab.com / www-gitlab-com project.
3. On the file page, click on the button labeled `Web IDE` near the middle of the page.
4. When prompted, click `Fork` to create a Fork of the repo which will allow you to make changes and submit a Merge Request (if you already create a fork upfront make sure to [synchronize it](https://about.gitlab.com/blog/2016/12/01/how-to-keep-your-fork-up-to-date-with-its-origin/) to see the latest version).
5. You should see the `heroes_contributions.yml` file open in your browser once the fork has been created. If using an existing fork of the www-gitlab-com project, open the [heroes_contributions](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/heroes_contributions.yml) file. 
6. Add a new contribution entry to the top of heroes_contributions.yml file by copying and pasting the below fields to the document starting on line 2: 
``` 
  - title: 
    contributor: 
    type: 
    logo: 
    link: 
    date: 
```
7. Update each of the blank fields following these guidelines:
  * title: title of your talk, blog post, video, or other contribution
  * contributor: include your first name and last name 
  * type: should be "Video", "Blog post", "Tech talk", or "Code"
  * logo: Select the file from `images/heroes/contributions-icons` directory that matches your choice from the `type` field. Please use:
    * `/images/heroes/contribution-icons/tech-talk.png` for Tech talks
    * `/images/heroes/contribution-icons/video.png` for Videos
    * `/images/heroes/contribution-icons/blog.png` for Blog posts
    * `/images/heroes/contribution-icons/code.png` for Code contributions
  * link: include a link to the contribution location or Merge Request 
  * date: date of publishing for blog posts and videos, date of the event for tech talks, and date of merge for code contributions
8. Once you have finished adding your contribution, click the `Commit` button in the bottom left. It should say something like `1 unstaged and 0 staged changes`. This will bring up a sidebar with an `Unstaged` and `Staged` area.
9. Check the files to ensure your updates are what you expect. If they are, click the check mark next to the filename to "stage" these changes.
10. Once you have verified all of the edits, enter a short commit message including what you've changed. Choose `Create a new branch`. Name the branch in the format of `YOURINITIALS-add-contribution` or similar. Tick the `Start a new merge request` checkbox. Then click `Commit` once more.
11. Click on the Activity link in the header to go to your Activity page. Once there, click on the blue `Create merge request button` at the top of the page.
12. Fill out the merge request details. Please ensure you tick the box to `Allow commits from members who can merge to target branch` as detailed on the [Allow collaboration on merge requests across forks](https://docs.gitlab.com/ee/user/project/merge_requests/allow_collaboration.html#enabling-commit-edits-from-upstream-members) page in our docs.
13. Mention @johncoghlan in a comment in the merge request so our team can review and merge.

# Requesting support from Heroes

Heroes are available to support the GitLab community as their time permits. Community members are invited to request the support of a GitLab Hero, whether for an event, a blog post, an interview, or another opportunity. 
* To request support from GitLab Heroes, please create an issue using the [Request for Heroes template](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes/issues/new?issuable_template=request-for-heroes).
* To review open requests, please review [issues with the `request-for-heroes` label](https://gitlab.com/-/ide/project/gitlab-com/marketing/community-relations/gitlab-heroes/blob/master/-/.gitlab/issue_templates/request-for-heroes.md).  

# Communication 

Please communicate using MRs and Issues in the GitLab Heroes project whenever possible to provide transparency to the wider GitLab community and allow for easier collaboration. If you need to reach the Evangelist Program Manager from GitLab, please email evangelists@gitlab.com. 

## Monthly Update 

The Evangelist Program Manager creates a monthly update issue ([template](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes/issues/new?issuable_template=monthly-update)) following the month's Steering Committe meeting. The issue should also be distributed to the Heroes via email. 

## Gitter

There is a [GitLab Heroes room](https://gitter.im/gitlabhq/heroes#) in the GitLab Gitter community that is used to chat about upcoming events, Meetups, GitLab Heroes, and other non-code topics. Recent issues and merge requests from the GitLab Heroes project appear in the activity section in the right sidebar. 

## Coffee Chats

GitLab Heroes follow the GitLab practice of using [coffee chats](https://about.gitlab.com/company/culture/all-remote/informal-communication/#coffee-chats) to encourage informal communication and promote stronger bonds between members of the program. Please use the [Heroes Coffee Chat](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes/issues/new?issuable_template=heroes-coffee-chat) issue template if you want to plan a coffee chat. Upcoming coffee chats are typically announced in our [Monthly Update](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes#monthly-update) and [Gitter](https://gitter.im/gitlabhq/heroes#). 

# Questions / Feedback
If anyone has any thoughts, concerns, ideas for how we can improve the program, please open an Issue here or create a Merge Requests (to this project, the GitLab Heroes page, and the GitLab Handbook)! Thanks!  

# Goals of GitLab Heroes

During a Steering Committee's call we decided to figure out the goals of the GitLab Heroes program. [This issue was opened](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-heroes/issues/35) to kick off discussion.
The issue has been broken up into various sections of the repo. Each of these topics need discussion and conclusions populated into the relevant section.

* [Goals/experience of the Heroes program](goals/README.md)
* [How the steering committee can help to achieve/enhance this](steering_committee/README.md)
* [Additional reward suggestions for Heroes](rewards/README.md)
* [Guidelines and course topics to be discussed in meetups, videos, blogs and within the Hero platform](content-guidelines/README.md)
